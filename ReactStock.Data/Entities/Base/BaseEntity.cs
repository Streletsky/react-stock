﻿using System.ComponentModel.DataAnnotations;

namespace ReactStock.Data.Entities.Base
{
    /// <summary>
    ///     Serves as base class for all business entities.
    /// </summary>
    public abstract class BaseEntity : IEntity<int>
    {
        #region Constants

        /// <summary>
        ///     To help ensure hash code uniqueness, a carefully selected random number multiplier
        ///     is used within the calculation. Goodrich and Tamassia's Data Structures and
        ///     Algorithms in Java asserts that 31, 33, 37, 39 and 41 will produce the fewest number
        ///     of collissions. See http://computinglife.wordpress.com/2008/11/20/why-do-hash-functions-use-prime-numbers/
        ///     for more information.
        /// </summary>
        private const int HashMultiplier = 31;

        #endregion


        #region Fields

        private int? cachedHashcode;

        #endregion


        #region Public Properties

        [Key][Required] public int Id { get; set; }

        #endregion


        #region Public Methods and Operators

        /// <summary>
        ///     Determine whether the specified <see cref="System.Object" /> is equal to this instance.
        /// </summary>
        /// <param name="obj">
        ///     The <see cref="object" /> to compare with the current <see cref="object" />.
        /// </param>
        /// <returns>
        ///     <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            var compareTo = obj as BaseEntity;

            if (ReferenceEquals(this, compareTo)) return true;

            if (compareTo == null || !(GetType() == compareTo.GetType())) return false;

            return Id.Equals(compareTo.Id);
        }

        /// <summary>
        ///     Return a hash code for this instance.
        /// </summary>
        /// <returns>
        ///     A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.
        /// </returns>
        /// <remarks>
        ///     This is used to provide the hash code identifier of an object using the signature
        ///     properties of the object; although it's necessary for NHibernate's use, this can
        ///     also be useful for business logic purposes and has been included in this base
        ///     class, accordingly. Since it is recommended that GetHashCode change infrequently,
        ///     if at all, in an object's lifetime, it's important that properties are carefully
        ///     selected which truly represent the signature of an object.
        /// </remarks>
        public override int GetHashCode()
        {
            if (cachedHashcode.HasValue) return cachedHashcode.Value;

            unchecked
            {
                // It's possible for two objects to return the same hash code based on
                // identically valued properties, even if they're of two different types,
                // so we include the object's type in the hash calculation
                int hashCode = GetType().GetHashCode();
                cachedHashcode = (hashCode * HashMultiplier) ^ Id.GetHashCode();
            }

            return cachedHashcode.Value;
        }

        #endregion
    }
}
