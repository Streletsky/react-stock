﻿namespace ReactStock.Data.Entities.Base
{
    /// <summary>
    ///     Represents interface of abstract entity in system.
    /// </summary>
    /// <typeparam name="TId">Type of unique identifier (ID) of entity.</typeparam>
    public interface IEntity<out TId>
    {
        #region Public Properties

        /// <summary>
        ///     Unique identifier (ID) of entity.
        /// </summary>
        TId Id { get; }

        #endregion
    }
}