﻿using ReactStock.Data.Entities.Base;

namespace ReactStock.Data.Entities
{
    public class User : BaseEntity
    {
        public string Email { get; set; }

        public string Name { get; set; }

        public string Subject { get; set; }

        public string Issuer { get; set; }
    }
}
