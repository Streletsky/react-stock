﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using ReactStock.Data.Entities;

namespace ReactStock.Data.OrmInfrastructure
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        public DbSet<User> Users { get; set; }
    }
}
