﻿using System.Linq;
using System.Threading.Tasks;
using ReactStock.Data.Entities.Base;

namespace ReactStock.Data.Repositories.Base
{
    /// <summary>
    ///     Represents interface to interact with data storage.
    /// </summary>
    /// <typeparam name="TEntity">
    ///     Type of entity repository will work with.
    /// </typeparam>
    /// <typeparam name="TId">
    ///     Type of ID of <typeparamref name="TEntity" />.
    /// </typeparam>
    public interface IRepository<TEntity, in TId> where TEntity : IEntity<TId>
    {
        #region Public Methods and Operators

        /// <summary>
        ///     Delete <typeparamref name="TEntity" /> entity from storage by ID.
        /// </summary>
        /// <param name="id">
        ///     ID of <typeparamref name="TEntity" /> entity.
        /// </param>
        /// <returns>
        ///     <c>true</c> if <typeparamref name="TEntity" /> entity was deleted successfully.
        /// </returns>
        Task DeleteAsync(TId id);

        /// <summary>
        ///     Delete given <typeparamref name="TEntity" /> entity from storage.
        /// </summary>
        /// <param name="entity">
        ///     <typeparamref name="TEntity" /> entity to delete.
        /// </param>
        /// <returns>
        ///     <c>true</c> if <typeparamref name="TEntity" /> entity was deleted successfully.
        /// </returns>
        Task DeleteAsync(TEntity entity);

        /// <summary>
        ///     Get all <typeparamref name="TEntity" /> entities from storage.
        /// </summary>
        /// <returns>
        ///     All <typeparamref name="TEntity" /> entities.
        /// </returns>
        IQueryable<TEntity> GetAll();

        /// <summary>
        ///     Get <typeparamref name="TEntity" /> entity from storage by ID.
        /// </summary>
        /// <param name="id">
        ///     ID of <typeparamref name="TEntity" />.
        /// </param>
        /// <returns>
        ///     Found <typeparamref name="TEntity" /> entity.
        /// </returns>
        Task<TEntity> GetAsync(TId id);

        /// <summary>
        ///     Create <typeparamref name="TEntity" /> entity in storage.
        /// </summary>
        /// <param name="entity">
        ///     <typeparamref name="TEntity" /> entity to create.
        /// </param>
        /// <returns>
        ///     <c>true</c> if <typeparamref name="TEntity" /> entity was inserted successfully.
        /// </returns>
        Task<TEntity> InsertAsync(TEntity entity);

        /// <summary>
        ///     Update given <typeparamref name="TEntity" /> entity in storage.
        /// </summary>
        /// <param name="entity">
        ///     <typeparamref name="TEntity" /> entity to update.
        /// </param>
        /// <returns>
        ///     <c>true</c> if <typeparamref name="TEntity" /> entity was updated successfully.
        /// </returns>
        Task<TEntity> UpdateAsync(TEntity entity);

        #endregion
    }
}
