﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ReactStock.Data.Entities;
using ReactStock.Data.OrmInfrastructure;
using ReactStock.Data.Repositories.Base;

namespace ReactStock.Data.Repositories
{
    public class UserRepository : EfRepository<User, int>, IUserRepository
    {
        public UserRepository(AppDbContext context) : base(context) { }

        public async Task<User> GetByEmailAsync(string email) => await EntitiesSet.FirstOrDefaultAsync(u => u.Email == email);
    }

    public interface IUserRepository : IRepository<User, int>
    {
        Task<User> GetByEmailAsync(string email);
    }
}
