﻿namespace ReactStock.Services.Security
{
    public interface ISecurityService
    {
        string Decrypt(string key, string cipherString, bool useHashing = true);

        string Encrypt(string key, string toEncrypt, bool useHashing = true);
    }
}