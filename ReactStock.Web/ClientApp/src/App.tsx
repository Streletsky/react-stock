import * as React from 'react'
import { Route, Switch } from 'react-router'
import { connect } from 'react-redux'
import Layout from './components/Layout'
import Watchlist from './containers/Watchlist'
import Details from './containers/Details'
import NotFound from './containers/NotFound'
import { ApplicationState } from './store/states'
import Login from './containers/Login'
import PrivateRoute from './components/PrivateRoute'

interface StateFromProps {
    isAuthenticated: boolean
}

const AppComponent = (props: StateFromProps) => {
    const { isAuthenticated } = props

    return (
        <Layout>
            <Switch>
                <PrivateRoute exact path="/" component={Watchlist} isAuthenticated={isAuthenticated} />
                <PrivateRoute path="/details:ticker" component={Details} isAuthenticated={isAuthenticated} />
                <Route path="/login" component={Login} />
                <Route component={NotFound} />
            </Switch>
        </Layout>
    )
}

const mapStateToProps = (state: ApplicationState) => ({
    isAuthenticated: state.auth.isAuthenticated,
})

const App = connect(mapStateToProps, null)(AppComponent)
export default App
