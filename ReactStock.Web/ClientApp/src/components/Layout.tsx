import React, { ComponentProps, ReactElement } from 'react'
import { Container } from 'reactstrap'
import NavMenu from './NavMenu'

const Layout = (props: ComponentProps<'div'>): ReactElement => {
    const { children } = props

    return (
        <>
            <NavMenu />
            <Container>{children}</Container>
        </>
    )
}

export default Layout
