import {
    Collapse, Container, Navbar, NavbarBrand, NavbarToggler, NavItem,
} from 'reactstrap'
import { Link } from 'react-router-dom'
import React, { useState } from 'react'
import { GoogleLogout } from 'react-google-login'
import * as config from '../config.json'

function NavMenu() {
    const [isOpen, setIsOpen] = useState(false)
    const toggle = () => setIsOpen(!isOpen)

    const googleLogoutSuccess = () => {

    }

    return (
        <header>
            <Navbar className="navbar-expand-sm navbar-toggleable-sm border-bottom box-shadow mb-3" light>
                <Container>
                    <NavbarBrand tag={Link} to="/">
                        ReactStock.Web
                    </NavbarBrand>
                    <NavbarToggler onClick={toggle} className="mr-2" />
                    <Collapse className="d-sm-inline-flex flex-sm-row-reverse" isOpen={isOpen} navbar>
                        <ul className="navbar-nav flex-grow">
                            <NavItem>
                                <GoogleLogout
                                    clientId={config.GOOGLE_CLIENT_ID}
                                    buttonText="Logout"
                                    onLogoutSuccess={googleLogoutSuccess}
                                />
                            </NavItem>
                        </ul>
                    </Collapse>
                </Container>
            </Navbar>
        </header>
    )
}

export default NavMenu
