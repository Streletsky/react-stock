﻿import { Redirect, Route } from 'react-router'
import React from 'react'

const PrivateRoute = ({ component, isAuthenticated, ...rest }: any) => (
    <Route
        {...rest}
        render={(props) => (isAuthenticated === true
            ? React.createElement(component, props)
            : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />)}
    />
)

export default PrivateRoute
