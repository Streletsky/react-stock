import React from 'react'
import { shallow, ShallowWrapper } from 'enzyme'
import GoogleLogin from 'react-google-login'
import { Redirect } from 'react-router'
import { LoginComponent } from './Login'

describe('<Login />', () => {
    let wrapper: ShallowWrapper

    describe('when not authenticated', () => {
        beforeEach(() => {
            wrapper = shallow(<LoginComponent isAuthenticated={false} />)
        })

        it('renders <GoogleLogin/> button', () => {
            expect(wrapper.find(GoogleLogin)).toBeTruthy()
        })
    })

    describe('when authenticated', () => {
        beforeEach(() => {
            wrapper = shallow(<LoginComponent isAuthenticated />)
        })

        it('renders <Redirect/> button', () => {
            expect(wrapper.find(Redirect)).toBeTruthy()
        })
    })
})
