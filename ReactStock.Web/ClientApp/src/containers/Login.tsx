﻿import * as React from 'react'
import { connect } from 'react-redux'
import { Redirect } from 'react-router'
import GoogleLogin, { GoogleLoginResponse, GoogleLoginResponseOffline } from 'react-google-login'
import { Dispatch } from 'redux'
import { ApplicationState } from '../store/states'
import * as config from '../config.json'
import { isGoogleLoginResponse, refreshTokenSetup } from '../services/authService'
import { login } from '../store/actions'
import { User } from '../types/User'

interface ComponentProps {
    isAuthenticated: boolean
    onLogin(user: User): void
}

export const LoginComponent = (props: ComponentProps) => {
    const { isAuthenticated, onLogin } = props

    const googleAuthSuccess = (response: GoogleLoginResponse | GoogleLoginResponseOffline) => {
        if (isGoogleLoginResponse(response)) {
            refreshTokenSetup(response, onLogin)
        } else {
            console.log('Google login functionality is offline')
        }
    }

    const googleAuthFailure = (error: any) => {
        console.log(error)
    }

    const content = isAuthenticated ? (
        <div>
            <Redirect to="/" />
        </div>
    ) : (
        <div>
            <GoogleLogin clientId={config.GOOGLE_CLIENT_ID} buttonText="Google Login" onSuccess={googleAuthSuccess} onFailure={googleAuthFailure} isSignedIn />
        </div>
    )

    return (
        <div>
            <h1>Login</h1>
            {content}
        </div>
    )
}

const mapStateToProps = (state: ApplicationState) => ({
    isAuthenticated: state.auth.isAuthenticated,
})

const mapDispatchToProps = (dispatch: Dispatch) => ({
    onLogin: (user: User) => {
        dispatch(login(user))
    },
})

const Login = connect(mapStateToProps, mapDispatchToProps)(LoginComponent)
export default Login
