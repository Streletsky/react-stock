﻿import { GoogleLoginResponse } from 'react-google-login'
import { User } from '../types/User'

export function isGoogleLoginResponse(obj: any): obj is GoogleLoginResponse {
    return obj.tokenObj !== undefined
}

async function validateGoogleAuth(response: GoogleLoginResponse) {
    const tokenBlob = new Blob([JSON.stringify({ tokenId: response.tokenId })], { type: 'application/json' })
    const options: RequestInit = {
        method: 'POST',
        body: tokenBlob,
        mode: 'cors',
        cache: 'default',
    }

    const r = await fetch('/api/auth/google', options)
    const user = await r.json()
    console.log(user)
}

export function refreshTokenSetup(response: GoogleLoginResponse, onLogin: (user: User) => void) {
    validateGoogleAuth(response).then(() => {
        let refreshTiming = (response.tokenObj.expires_in || 3600 - 5 * 60) * 1000
        const user = response.getBasicProfile()

        const refreshToken = async () => {
            const newAuthRes = await response.reloadAuthResponse()
            refreshTiming = (newAuthRes.expires_in || 3600 - 5 * 60) * 1000
            localStorage.setItem('authToken', newAuthRes.id_token)

            setTimeout(refreshToken, refreshTiming)
        }

        onLogin({ email: user.getEmail() })
        setTimeout(refreshToken, refreshTiming)
    })
}

export function getAuthHeader() {
    const authToken = localStorage.getItem('authToken')

    if (authToken) {
        const user = JSON.parse(authToken)
        return { Authorization: `Bearer ${user.token}` }
    }

    return {}
}
