import { User } from '../types/User'

export interface LoginAction {
    type: 'LOGIN'
    payload: User
}

export interface LogoutAction {
    type: 'LOGOUT'
}

export type KnownAuthAction = LoginAction | LogoutAction

export const login = (user: User): LoginAction => ({
    type: 'LOGIN',
    payload: user,
})

export const logout = (): LogoutAction => ({
    type: 'LOGOUT',
})
