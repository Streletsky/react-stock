import { AuthState } from './states'
import { KnownAuthAction } from './actions'

const defaultAuthState: AuthState = {
    isAuthenticated: false,
    user: null,
}

export const authReducer = (state = defaultAuthState, action: KnownAuthAction) => {
    switch (action.type) {
        case 'LOGIN':
            return { ...state, user: action.payload, isAuthenticated: true }
        case 'LOGOUT':
            return { ...state, user: null, isAuthenticated: false }
        default:
            return state
    }
}

export const reducers = {
    auth: authReducer,
}
