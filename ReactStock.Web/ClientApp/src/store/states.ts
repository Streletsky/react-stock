﻿import { User } from '../types/User'

export interface AuthState {
    user: User | null
    isAuthenticated: boolean
}

export interface ApplicationState {
    auth: AuthState
}
