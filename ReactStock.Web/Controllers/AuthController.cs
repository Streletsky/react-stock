﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Google.Apis.Auth;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using ReactStock.Data.Entities;
using ReactStock.Data.Repositories;
using ReactStock.Services.Security;
using ReactStock.Web.DataTransferObjects;

namespace ReactStock.Web.Controllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    public class AuthController : Controller
    {
        private readonly ISecurityService securityService;

        private readonly IUserRepository userRepository;

        private readonly IConfiguration configuration;

        public AuthController(IUserRepository userRepository, IConfiguration configuration, ISecurityService securityService)
        {
            this.userRepository = userRepository;
            this.configuration = configuration;
            this.securityService = securityService;
        }

        [AllowAnonymous]
        [HttpPost("google")]
        public async Task<IActionResult> Google([FromBody]UserDto userDto)
        {
            try
            {
                var payload = await GoogleJsonWebSignature.ValidateAsync(userDto.TokenId, new GoogleJsonWebSignature.ValidationSettings());
                var user = await userRepository.GetByEmailAsync(payload.Email);

                if (user is null)
                {
                    user = new User()
                    {
                        Name = payload.Name,
                        Email = payload.Email,
                        Subject = payload.Subject,
                        Issuer = payload.Issuer
                    };

                    await userRepository.InsertAsync(user);
                }

                var claims = new[]
                {
                    new Claim(JwtRegisteredClaimNames.Sub, securityService.Encrypt(configuration["AppSettings:JwtEmailEncryption"],user.Email)),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                };

                var key = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(configuration["AppSettings:JwtSecret"]));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

                var token = new JwtSecurityToken(String.Empty,
                                                 String.Empty,
                                                 claims,
                                                 expires: DateTime.Now.AddSeconds(60*60),
                                                 signingCredentials: creds);
                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token)
                });
            }
            catch (Exception ex)
            {
                BadRequest(ex.Message);
            }

            return BadRequest();
        }
    }
}
